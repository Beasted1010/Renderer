

@echo off

cd obj

gcc -c -I../inc ../src/main.c ../src/screen.c ../src/pixel.c ../src/bitmap.c ../src/common.c

cd ../lib

ar rcs librenderer.a ../obj/main.o ../obj/screen.o ../obj/pixel.o ../obj/bitmap.o^
                     ../obj/common.o

cd ..

gcc -L lib obj/main.o obj/screen.o obj/pixel.o obj/bitmap.o obj/common.o^
    -lrenderer -lgdi32

