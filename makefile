
OBJDIR = obj
INCDIR = inc
SRCDIR = src

OBJECTS = main.o bitmap.o common.o pixel.o screen.o
INCLUDES = bitmap.h common.h pixel.h screen.h

CFLAGS = -Iinc -Wall

OUT = run

#run : main.o common.o pixel.o screen.o
#	gcc -o run main.o common.o pixel.o screen.o


$(OUT) : $(OBJDIR)/$(OBJECTS) $(INCDIR)/$(INCLUDES)
	gcc $(OBJDIR)/$(OBJECTS) -o $@

obj/main.o :
	cd obj && gcc -c ../src/main.c

$(OBJDIR)/%.o : ../$(SRCDIR)/%.c
	cd obj && gcc -c $^ ../$(CFLAGS) -o $@

#main.o : main.c
#	gcc -c main.c

#common.o : common.c common.h
#	gcc -c common.c

#pixel.o : pixel.c pixel.h
#	gcc -c pixel.c

#screen.o : screen.c screen.h
#	gcc -c screen.c


