 

#ifndef SCREEN_H
#define SCREEN_H

#include <windows.h>
#include "pixel.h"
#include "bitmap.h"

// This file contains information regarding the following...
//  - Window Creation
//  - Window Update
//  - Window Redraw
//  - Window Resize
//  - Window Deletion


// This structure contains information regarding a Micrsoft Windows' window
struct MS_Window
{
    WNDCLASS wc;
    HWND hWnd;
    HDC deviceContext;

    RECT clientRect;

    struct Bitmap image;
};


// Get the rectangle of the bitmap
static int GetBitmapRect(struct Bitmap* bitmap, struct xRect* out)
{
    if(!bitmap || !out)
    {
        return 0;
    }

    out->left = 0;
    out->top = 0;

    out->right = out->left + bitmap->w;
    out->bottom = out->top + bitmap->h;

    return 1;
}

// Get the rectangle of the drawable region -> This is the client region's rectangle
static int GetDrawableRect(HWND hWnd, struct xRect* out)
{

}





// Create the MS_Window structure and populate it
struct MS_Window CreateMSWindow(WNDPROC windowProc, HINSTANCE hInst, const char* className);
// Update the window with the latest image. From source bitmap to destination client window.
void UpdateMSWindow(struct MS_Window* wnd);
int ResizeMSWindow(struct MS_Window* wnd);
int RedrawMSWindow();
// Destroy the window
void DestroyMSWindow(struct MS_Window* wnd);


#endif // SCREEN_H

