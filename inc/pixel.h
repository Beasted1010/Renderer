 
#ifndef PIXEL_H
#define PIXEL_H

#include <stdint.h>
#include "bitmap.h"
#include "common.h"

#define TRANSPARENT_MASK 0xFF000000

struct xPoint
{
    uint32_t x;
    uint32_t y;
};

struct xLine
{
    struct xPoint p1;
    struct xPoint p2;
};

struct xTriangle
{
    struct xPoint tipPoint;
    struct xLine baseLine;
};

struct xRect
{
    uint32_t left;
    uint32_t top;
    uint32_t right;
    uint32_t bottom;
};

struct xCircle
{
    uint32_t radius;
    struct xPoint center;
};

enum AlphaSetting
{
    AlphaIgnore,
    AlphaBinary
};

struct Texture
{
    struct Bitmap image;
    struct RGB chromaKey;
    struct xRect rect;

    // May want to scale the image to a new texture width/height
    uint32_t width;
    uint32_t height;

    char* fileLocation;
};

struct CoordinateSystem2D
{
    // Origin in terms of units from top left
    struct UnitPosition
    {
        uint16_t xUnitLoc;
        uint16_t yUnitLoc;
    } origin;

    // These define the spacing between whole number points in the respective direction
    uint16_t unitsWide;
    uint16_t unitsTall;

    struct RGB axesColor; // Color of x/y axis lines

    uint8_t showGridLines; // A flag that indicates whether or not to show grid lines
};


void ReadPixel32(const uint32_t* pixel, struct RGB* rgb);
void WritePixel32(uint32_t* pixel, const struct RGB* rgb);
void WritePixels32(struct Bitmap* bitmap, struct RGB* rgb);
void DrawQuarterCircle(struct Bitmap* bitmap, struct xCircle* circle, 
                       enum Quadrant quadrant, struct RGB* color);

void DrawCircle(struct Bitmap* bitmap, struct xCircle* circle, struct RGB* color);
void DrawRepeatSymbol(struct Bitmap* bitmap, struct xCircle* circle, uint32_t thickness, struct RGB* color);
void DrawLine(struct Bitmap* bitmap, struct xLine* line, struct RGB* color);

void DrawHorizontalLine(struct Bitmap* bitmap, 
                        uint32_t start_x, uint32_t start_y, uint32_t end_x, struct RGB* color);

void DrawVerticalLine(struct Bitmap* bitmap, 
                      uint32_t start_x, uint32_t start_y, uint32_t end_y, struct RGB* color);

void DrawTriangle(struct Bitmap* bitmap, struct xTriangle* triangle, struct RGB* color);
void FillTriangle(struct Bitmap* bitmap, struct xTriangle* triangle, struct RGB* color);
void DrawRectangle(struct Bitmap* bitmap, struct xRect* rect, struct RGB* color);
void FillRectangle(struct Bitmap* bitmap, struct xRect* rect, struct RGB* color);

struct CoordinateSystem2D CreateCoordinateSystem2D(uint16_t xUnitPxs, uint16_t yUnitPxs,
                                                   uint16_t unitOriginFromLeft, uint16_t unitOriginFromBottom,
                                                   struct RGB* axesColor);

void DrawCoordinateSystem2D(struct CoordinateSystem2D* coordSys, struct Bitmap* bitmap, 
                            uint16_t xStart, uint16_t yStart, uint16_t width, uint16_t height);
//void DrawCoordinateSystem2D(struct CoordinateSystem2D* coordSys, struct Bitmap* bitmap, 
//                            int xStart, int yStart, int width, int height);

struct Texture CreateTexture(char* fileLocation, struct RGB* chromaKey);
void DestroyTexture(struct Texture* texture);
void PlaceTexture(struct Texture* texture, struct Bitmap* bitmap, uint32_t xPos, uint32_t yPos, enum AlphaSetting alpha);
//void PlaceTextureScaled(struct Texture* texture, struct Bitmap* bitmap, uint32_t xPos, uint32_t yPos, uint8_t scaleFactor, enum AlphaSetting alpha);
void ScaleTexture(struct Texture* texture, float scaleFactor);











#endif // PIXEL_H
