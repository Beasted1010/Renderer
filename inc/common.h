#ifndef COMMON_H
#define COMMON_H

#include "stdio.h"
#include "stdint.h"

#define SCREEN_WIDTH 1920
#define SCREEN_HEIGHT 1080

#define MIN_SCREEN_WIDTH_PX 700
#define MIN_SCREEN_HEIGHT_PX 700

struct RGB
{
    uint8_t r;
    uint8_t g;
    uint8_t b;
    uint8_t a;
}; 

enum Quadrant
{
    INVALID = 0,
    TOP_RIGHT = 1,
    TOP_LEFT = 2,
    BOTTOM_LEFT = -2, // Negative values used only to provide means of determining if quadrant is above or below x axis
    BOTTOM_RIGHT = -1
};

// TODO: Make this inline...? Also rethink this whole deal..
int ValidateObject(void* ptr, char* name);

// TODO: Use Math library?
float AbsoluteValue( float val );
float Exponentiate( float base, float exponent );
float NthRoot( int nthRoot, float val );

#endif //COMMON_H


