
#ifndef BITMAP_H
#define BITMAP_H

#include <stdint.h>
#include <windows.h>
#include "common.h"

// Information contained within a bitmap's file header
struct BMPFileHeader
{
    char type[2];
    uint32_t fSize;

    // NOTE: uint16_t bfReserved1, bfReserved2; -> These exist in header, but have no practical use

    uint32_t bfOffBits;
};

// Color Palette information for a bitmap that supports it
struct ColorPalette
{
    uint8_t blue;
    uint8_t green;
    uint8_t red;
    uint8_t reserved; // Always 0
};

// Masking information for a compressed bitmap image
struct BMPMask
{
    uint32_t rMask, gMask, bMask, aMask;
    uint32_t rShift, gShift, bShift, aShift;
    uint32_t rMax, gMax, bMax, aMax;
};

// Information contained in a Version 3 bitmap header
struct BMP3Header
{
    uint32_t headerSize;
    int32_t w, h;
    uint16_t planes;
    uint16_t bitsPerPixel;
    uint32_t compression;
    uint32_t imageSize;

    int32_t xPelsPerMeter;
    int32_t yPelsPerMeter;

    uint32_t colorsUsed;
    uint32_t colorsImportant;
    struct BMPMask bitMask;
};

// The bitmap structure
struct Bitmap
{
    BITMAPINFO* info; // TODO: Maybe do away with this completely

    uint32_t w, h;
    void* pixels;

    uint32_t numPaletteColors;
    uint8_t bytesPerPixel; // We convert each pixel to 32 bits, but this member retains the original bytesPerPixel from bmp3Header
};







// Initializes the bitmap values
int CreateBitmapImage(struct Bitmap* bitmap, uint32_t width, uint32_t height);
// Destroys the bitmap, frees memory
void DestroyBitmapImage(struct Bitmap* bitmap);
// Resize the memory associated with a bitmap based on new width and height details
int ResizeBitmapMemory(struct Bitmap* bitmap, uint32_t width, uint32_t height);
// Load a compatible .bmp file's bitmap image
int LoadBitmapImage(const char* fileLocation, struct Bitmap* image, const struct RGB* chromaKey);


#endif // BITMAP_H
