

#include "common.h"


// Ensure that a pointer is pointing to some memory location
int ValidateObject(void* ptr, char* name)
{
    if(!ptr)
    {
        printf("Failed to allocate memory for %s!\n", name);
        // TODO: Failure stuff
    }
}

float AbsoluteValue( float val )
{
    if( val >= 0 )
        return val;
    else
        return -val;
}

float Exponentiate( float base, float exponent )
{
    int i;
    float result = 1;
    for( i = 0; i < exponent; i++ )
    { result *= base; }

    return result;
}

// Newton's method
float NthRoot( int nthRoot, float val )
{
    float threshold = 0.001;
    //printf("nthRoot = %i\nthreshold = %f\n\n", threshold);

    int num_iterations = 0;

    // TODO: NEED TO CALCULATE THIS, PERHAPS WITH SOME SORT OF SQUEEZE LIMIT THINGY? estimate.
    float guess = val / 2.0;
    int converged = 0;
    float f_x;
    float f_prime_x;

    float old_guess = guess;

    while(!converged)
    {
        f_x = Exponentiate(guess, nthRoot) - val; // Function x^n - i = 0, from x = nthRoot(i)
        f_prime_x = nthRoot * Exponentiate(guess, nthRoot - 1); // Power rule -> n * (x^(n-1))
        //printf("f_x = %f\nf_prime_x = %f\n\n", f_x, f_prime_x);

        old_guess = guess;
        guess = guess - (f_x / f_prime_x); // Update guess
        //printf("old_guess = %f\nnew guess = %f\n\n", old_guess, guess);

        num_iterations++;

        //printf("Change = %f\n", AbsoluteValue(old_guess - guess));
        if( AbsoluteValue(old_guess - guess) < threshold)
            converged = 1;
    }

    //printf("%i root of %f = %f", nthRoot, val, guess);
    //printf("Calculation took %i iterations\n", num_iterations);

    return guess;
}




