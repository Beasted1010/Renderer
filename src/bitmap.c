
#include <stdio.h>
#include "bitmap.h"
#include "pixel.h"

int CreateBitmapImage(struct Bitmap* bitmap, uint32_t width, uint32_t height)
{
    bitmap->w = 0;
    bitmap->h = 0;

    bitmap->info = malloc(sizeof(BITMAPINFO));

    bitmap->info->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
    bitmap->info->bmiHeader.biPlanes = 1;
    bitmap->info->bmiHeader.biBitCount = 32;
    bitmap->info->bmiHeader.biCompression = BI_RGB;

    // All zero/null
    bitmap->info->bmiHeader.biSizeImage = 0;
    bitmap->info->bmiHeader.biXPelsPerMeter = 0;
    bitmap->info->bmiHeader.biYPelsPerMeter = 0;
    bitmap->info->bmiHeader.biClrUsed = 0;
    bitmap->info->bmiHeader.biClrImportant = 0;

    // TODO: We only support 4 bytes per pixel. But this should probably be more flexible? Would need adjusting to "PlaceTexture" in pixel.c
    bitmap->bytesPerPixel = 4;
    //bitmap->bytesPerPixel = bitmap->info->bmiHeader.biBitCount / 8;

    bitmap->pixels = 0;

    return ResizeBitmapMemory(bitmap, width, height);
}

void DestroyBitmapImage(struct Bitmap* bitmap)
{
    free(bitmap->info);

    VirtualFree(bitmap->pixels, 0, MEM_RELEASE);
    bitmap->pixels = 0;
    bitmap->w = bitmap->h = 0;
}

int ResizeBitmapMemory(struct Bitmap* bitmap, uint32_t width, uint32_t height)
{
    if(bitmap->pixels)
    {
        VirtualFree(bitmap->pixels, 0, MEM_RELEASE);
    }

    bitmap->info->bmiHeader.biWidth = width;
    bitmap->info->bmiHeader.biHeight = -height; // I want bitmap rendered top down (so want negative height)
    bitmap->w = width;
    bitmap->h = height;

    //printf("width = %i\theight = %i\t bytesPP = %i\n", width, height, bitmap->bytesPerPixel);
    bitmap->info->bmiHeader.biSizeImage = (width * height) * (bitmap->bytesPerPixel);

    if(bitmap->info->bmiHeader.biSizeImage == 0)
    {
        printf("Can't allocate pixel memory with a size of 0 bytes\n");
        return 0;
    }

    bitmap->pixels = VirtualAlloc(0, bitmap->info->bmiHeader.biSizeImage, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);

    if(!ValidateObject(bitmap->pixels, "bitmap->pixels")) { return 0; }


    memset(bitmap->pixels, 0, bitmap->info->bmiHeader.biSizeImage);

    return 1;
}

void PrintBMFHeader(struct BMPFileHeader* fileHeader)
{
    printf("BITMAP FILE HEADER...\n");
    printf("---------------------------------------------------\n");
    printf("Type... Should be characters \"BM\" for Bitmap file: %c%c\n", fileHeader->type[0], fileHeader->type[1]);
    printf("Size of file in bytes: %i\n", fileHeader->fSize);
    printf("Offset to start of pixel data: %i\n", fileHeader->bfOffBits);
    printf("---------------------------------------------------\n");
}

void PrintBMIHeader(struct BMP3Header bmiHeader)
{
    printf("BITMAP INFO HEADER FILE...\n");
    printf("---------------------------------------------------\n");
    printf("Size of bitmap header: %i\n", bmiHeader.headerSize);
    printf("Image width in pixels: %i\n", bmiHeader.w);
    printf("Image height in pixels: %i\n", bmiHeader.h);
    printf("Number of planes (must be 1): %i\n", bmiHeader.planes);
    printf("Bits per pixel: %i\n", bmiHeader.bitsPerPixel);
    printf("Compressoin type (0 = uncompressed): %i\n", bmiHeader.compression);
    printf("Size of image (may be 0 for uncompressed images): %i\n", bmiHeader.imageSize);
    printf("Preferred resolution in pixels per meter (x dir): %i\n", bmiHeader.xPelsPerMeter);
    printf("Preferred resolution in pixels per meter (y dir): %i\n", bmiHeader.yPelsPerMeter);
    printf("Number color map entries that are actually used: %i\n", bmiHeader.colorsUsed);
    printf("Number of significant colors: %i\n", bmiHeader.colorsImportant);
    printf("---------------------------------------------------\n");

    if(bmiHeader.headerSize == 108)
    {
        printf("BITMAP IS V4! IT CONTAINS THE FOLLOWING ADDITIONAL DATA!\n");
        printf("---------------------------------------------------\n");
        //printf("RedMask = %u\n", bmiHeader.biRedMask);
    }
}

static inline void SwapBitPlacement(unsigned char* image, int width, int height, uint8_t bpp, uint32_t colorUsed)
{
    // If height > 0 then the image is bottom-up. Our bitmap assumes top down, so adjust for the image
    int image_index = (height-1) * width * (bpp / 8);
    int index_adjust = 2 * -width * (bpp / 8);
    int byte_swapper;
    int left_counter = 0;

    struct RGB rgb = {.a=0, .r=0, .g=0, .b=0};
    for(int image_row = 0; image_row < height / 2; image_row++)
    {
        for(int image_col = 0; image_col < width; image_col++)
        {
            if(colorUsed) // Assuming 1 byte per pixel ... TODO: Ensure this to be the case
            {
                byte_swapper = image[left_counter];
                image[left_counter++] = image[image_index];
                image[image_index++] = byte_swapper;
            }
            else // Assuming 3 bytes per pixel ... TODO: Ensure this to be the case?
            {
                // Do this swapping for the next 3 bytes
                for(int k = 0; k < 3; k++)
                {
                    byte_swapper = image[left_counter];
                    image[left_counter++] = image[image_index];
                    image[image_index++] = byte_swapper;
                }
            }

            //printf("a = %i\tr = %i\tg = %i\tb = %i\n", rgb.a, rgb.r, rgb.g, rgb.b);
            //getchar();
        }
        image_index += index_adjust;
    }
}

static inline uint16_t SwapByteOrder16(uint16_t byte)
{
    return ( ((byte && 0xFF00) >> 8) |
             ((byte && 0x00FF) << 8) );
}

static inline uint32_t SwapByteOrder32(uint32_t byte)
{
    return ( ((byte && 0xFF000000) >> 24) |
             ((byte && 0x00FF0000) >> 8) |
             ((byte && 0x0000FF00) << 8) |
             ((byte && 0x000000FF) << 24) );
}

// A debugging friendly means to print out data byte by byte
// NOTE: Generally a bad idea since you may attempt to read memory not belonging to you...
void StepThroughByteData(uint8_t* bytes, uint32_t startIndex)
{
    int ch = 0;
    printf("Reading bytes... x to stop\n");
    while( ch != 'x' )
    {
        printf("Byte index of %u has value: %u\n", startIndex, bytes[startIndex++]);
        ch = getchar();
        //while( (ch == getchar()) != '\n' && ch != EOF ) ; // Flushing input buffer
    }
}

// Reference: BrainSteel's "sandwich-boogaloo" game -> https://github.com/BrainSteel/sandwich-boogaloo/blob/master/src/pixel.c
// Read a byte from a stream of data, update the offset.
// Converting value at memory location to pointer to type, then derefrencing to get value at location
// Assuming little-endian architecture
#define READ_BYTESTREAM( type, ptr, offset) (* (type*)(ptr + offset)); offset += sizeof(type);

// Objective 1: Read a .bmp file's header information (fileHeader, infoHeader & masking data, colorPalette?)
// Objective 2: Read .bmp image data, store as a 32 bit pixel => WriteRGB for 24 bit 
// Bitmap format -> File Header, Bitmap Info Header, Masking info, Color Palette, Bitmap Data -> Some may be left out
// TODO: Need to support bitmap v5
int LoadBitmapImage(const char* fileLocation, struct Bitmap* image, const struct RGB* chromaKey)
{
    FILE* fp = fopen(fileLocation, "rb");
    ValidateObject(fp, "Bitmap image file pointer");

//void WritePixel32(uint32_t* pixel, struct RGB* rgb)
    uint32_t mapped_color_key;
    if(chromaKey)
    {
        WritePixel32(&mapped_color_key, chromaKey);
    }

    // Read the first 14 bytes of the header to ensure the file is a .bmp file and to obtain the file's size
    const uint32_t head_size = 14;
    char filehead_bytes[head_size];
    if( !fread(&filehead_bytes, 1, head_size, fp) )
    {
        printf("The file was unable to be read!\n");
        fclose(fp);
        return 0;
    }

    // The offset for our location in any chunk of bytes that we are currently focusing on
    int file_offset = 0, offset = 0;

    struct BMPFileHeader filehead;
    filehead.type[0] = READ_BYTESTREAM(char, filehead_bytes, offset);
    filehead.type[1] = READ_BYTESTREAM(char, filehead_bytes, offset);

    if( filehead.type[0] != 'B' || filehead.type[1] != 'M' )
    {
        printf("File type of %s is not support! First 2 characters of type read: %c%c\n", 
                                        fileLocation, filehead.type[0], filehead.type[1]);
        fclose(fp);
        return 0;
    }

    filehead.fSize = READ_BYTESTREAM(uint32_t, filehead_bytes, offset);
    
    // Ignore the next two words, they have no practical use
    offset += 2 * sizeof(uint16_t);

    // This is the location where the image data begins
    filehead.bfOffBits = READ_BYTESTREAM(uint32_t, filehead_bytes, offset);

    // Reset the offset so that we may begin reading data into the bmp3Header
    file_offset += offset;
    offset = 0;
    struct BMP3Header bmp3Header; // Structure to store all the header data that we come across
    
    // Assume to be 40, if we end up reading more than 40 bytes then things will be fine
    const uint32_t bmphead_size = 40;
    char bmphead_bytes[bmphead_size];

    if( !fread(&bmphead_bytes, 1, bmphead_size, fp) )
    {
        printf("Could not read file header of %s!", fileLocation);
        fclose(fp);
        return 0;
    }

    bmp3Header.headerSize = READ_BYTESTREAM(uint32_t, bmphead_bytes, offset);

    if( bmp3Header.headerSize < 40 )//|| bmp3Header.headerSize > 108 )
    {
        printf("File %s is of some unsupported BMP version, got %u header bytes\n", fileLocation, bmp3Header.headerSize);
        fclose(fp);
        return 0;
    }

    uint8_t is_v4 = bmp3Header.headerSize > 40;

    bmp3Header.w = READ_BYTESTREAM(int32_t, bmphead_bytes, offset);
    bmp3Header.h = READ_BYTESTREAM(int32_t, bmphead_bytes, offset);
    bmp3Header.planes = READ_BYTESTREAM(uint16_t, bmphead_bytes, offset);
    bmp3Header.bitsPerPixel = READ_BYTESTREAM(uint16_t, bmphead_bytes, offset);
    bmp3Header.compression = READ_BYTESTREAM(uint32_t, bmphead_bytes, offset);
    bmp3Header.imageSize = READ_BYTESTREAM(uint32_t, bmphead_bytes, offset);
    bmp3Header.xPelsPerMeter = READ_BYTESTREAM(int32_t, bmphead_bytes, offset);
    bmp3Header.yPelsPerMeter = READ_BYTESTREAM(int32_t, bmphead_bytes, offset);
    bmp3Header.colorsUsed = READ_BYTESTREAM(uint32_t, bmphead_bytes, offset);
    bmp3Header.colorsImportant = READ_BYTESTREAM(uint32_t, bmphead_bytes, offset);

    // Prepare offset for next chunk of bytes
    file_offset += offset;
    //printf("file offset = %i\n", file_offset);
    //printf("offset = %i\n", offset);
    //offset = 0;
    //printf("Location in file: %i\n", ftell(fp));

uint32_t save_off = offset;
//PrintBMIHeader(bmp3Header);

    bmp3Header.bitMask.rMask = bmp3Header.bitMask.gMask = bmp3Header.bitMask.bMask = bmp3Header.bitMask.aMask = 0;
    bmp3Header.bitMask.rShift = bmp3Header.bitMask.gShift = bmp3Header.bitMask.bShift = bmp3Header.bitMask.aShift = 0;
    bmp3Header.bitMask.rMax = bmp3Header.bitMask.gMax = bmp3Header.bitMask.bMax = bmp3Header.bitMask.aMax = 0;
    if( bmp3Header.compression == 3 || is_v4 )
    {
        char mask_bytes[4];
        const uint32_t mask_size = is_v4 ? 4 * sizeof(uint32_t) : 3 * sizeof(uint32_t);
        if( !fread(&mask_bytes, 1, mask_size, fp) )
        {
            printf("Could not read bitmasks of %s!", fileLocation);
            fclose(fp);
            return 0;
        }

    //StepThroughByteData(mask_bytes, offset);
    //StepThroughByteData(bmphead_bytes, save_off);
        // TODO: using bmphead_bytes for now since mask_bytes seems to put me in the wrong spot?
        // TODO: Need to figure out what is going on with mask_bytes. I am not offset=0 to continue with bmphead_bytes (for now)
        bmp3Header.bitMask.rMask = READ_BYTESTREAM(uint8_t, bmphead_bytes, offset);
        bmp3Header.bitMask.gMask = READ_BYTESTREAM(uint8_t, bmphead_bytes, offset);
        bmp3Header.bitMask.bMask = READ_BYTESTREAM(uint8_t, bmphead_bytes, offset);

        bmp3Header.bitMask.rShift = __builtin_ffs(bmp3Header.bitMask.rMask) - 1; 
            if( __builtin_ffs(bmp3Header.bitMask.rMask) == 0) bmp3Header.bitMask.rShift = 0;
        bmp3Header.bitMask.gShift = __builtin_ffs(bmp3Header.bitMask.gMask) - 1; 
            if( __builtin_ffs(bmp3Header.bitMask.gMask) == 0) bmp3Header.bitMask.gShift = 0;
        bmp3Header.bitMask.bShift = __builtin_ffs(bmp3Header.bitMask.bMask) - 1; 
            if( __builtin_ffs(bmp3Header.bitMask.bMask) == 0) bmp3Header.bitMask.bShift = 0;

        bmp3Header.bitMask.rMax = bmp3Header.bitMask.rMask >> bmp3Header.bitMask.rShift;
        bmp3Header.bitMask.gMax = bmp3Header.bitMask.gMask >> bmp3Header.bitMask.gShift;
        bmp3Header.bitMask.bMax = bmp3Header.bitMask.bMask >> bmp3Header.bitMask.bShift;

        if( is_v4 )
        {
            bmp3Header.bitMask.aMask = READ_BYTESTREAM(uint8_t, bmphead_bytes, offset);
            bmp3Header.bitMask.aShift = __builtin_ffs(bmp3Header.bitMask.aMask) - 1;
                if( __builtin_ffs(bmp3Header.bitMask.aMask) == 0) bmp3Header.bitMask.aShift = 0;
            
            if( bmp3Header.bitMask.aShift < 0 )
            {
                bmp3Header.bitMask.aShift = 0;
                bmp3Header.bitMask.aMax = 0;
            }
            else
            {
                bmp3Header.bitMask.aMax = bmp3Header.bitMask.aMask >> bmp3Header.bitMask.aShift;
            }
        }
        /*printf("rMask = %i\n", bmp3Header.bitMask.rMask);
        printf("gMask = %i\n", bmp3Header.bitMask.gMask);
        printf("bMask = %i\n", bmp3Header.bitMask.bMask);
        printf("aMask = %i\n", bmp3Header.bitMask.aMask);
        printf("r shift = %i\n", bmp3Header.bitMask.rShift);
        printf("g shift = %i\n", bmp3Header.bitMask.gShift);
        printf("b shift = %i\n", bmp3Header.bitMask.bShift);
        printf("a shift = %i\n", bmp3Header.bitMask.aShift);
        printf("rmax = %i\n", bmp3Header.bitMask.rMax);
        printf("gmax = %i\n", bmp3Header.bitMask.gMax);
        printf("bmax = %i\n", bmp3Header.bitMask.bMax);
        printf("amax = %i\n", bmp3Header.bitMask.aMax);*/
    }

    // Move file pointer to the end of the header sections, this ensures we are looking at the data that immediately follows
    uint16_t header_bytes = sizeof(BITMAPFILEHEADER) + bmp3Header.headerSize;
    fseek(fp, header_bytes, SEEK_SET);

    uint8_t color_palette_size;
    if( bmp3Header.headerSize == 12 ) // We have a Windows 2.x BMP file (possible OS/2 1.x)
    {
        color_palette_size = 3;
    }
    else
    {
        color_palette_size = 4;
    }

    // Calculate the number of color palette entries
    // This is manually calculated to allow future support of Windows 2.x BMPs (because why not)
    //      Otherwise this information is already available in the "colorsUsed" field of the BMP Header
    int16_t num_palette_entries = (filehead.bfOffBits - sizeof(BITMAPFILEHEADER) -
                                    bmp3Header.headerSize) / color_palette_size;

    if( bmp3Header.headerSize >= 40 && bmp3Header.colorsUsed != num_palette_entries )
    {
        printf("There was a problem in the calculation of num_palette_entries!...\n");
        printf("num_palette_entries = %i and colorsUsed = %u\n", num_palette_entries, bmp3Header.colorsUsed);
        fclose(fp);
        return 0;
    }

    uint32_t* palette_entries = malloc( (sizeof(uint8_t) * color_palette_size) * num_palette_entries);
    ValidateObject(palette_entries, "palette_entries");
    if( num_palette_entries )
    {
        if(!fread(palette_entries, 1, num_palette_entries, fp))
        {
            printf("Failed to read the color palette entries!\n");
            free(palette_entries);
            fclose(fp);
            return 0;
        }
    }
    else
    {
        free(palette_entries);
        palette_entries = NULL;
    }

    // REGION: Reading the actual image data now
    // Ensure we have positive values for width and height (-height = top down bitmap)
    uint32_t real_w = bmp3Header.w < 0 ? -bmp3Header.w : bmp3Header.w;
    uint32_t real_h = bmp3Header.h < 0 ? -bmp3Header.h : bmp3Header.h;

    // Initialize the bitmap members
    CreateBitmapImage(image, real_w, real_h);

    image->numPaletteColors = num_palette_entries;

    int y_dir = 1;
    int y_start = 0;
    // If our bitmap is bottom up, then we want to reverse the direction (we want top down)
    if(bmp3Header.h > 0)
    {
        y_dir = -1;
        y_start = real_h - 1; // Start at the last row
    }

    uint32_t bytes_pp = bmp3Header.bitsPerPixel / 8;
    int pitch = bytes_pp * real_w;
    uint32_t pitch_mod = pitch % 4; // How far are we from a boundary of 4 bytes?
    if( pitch_mod ) // Do we have any distance from a boundary of 4?
    {
        // Add back how far away we are from 4 so that we are on a 32-bit boundary
        pitch += 4 - pitch_mod;
    }

    const uint32_t bmp_size = real_h * pitch; // Number of bytes we will be reading in for our image

    uint8_t* raw_image_bytes = (uint8_t*) malloc(sizeof(uint8_t) * bmp_size);
    ValidateObject(raw_image_bytes, "raw_image_bytes");
    
    fseek(fp, filehead.bfOffBits, SEEK_SET);
    if( !fread(raw_image_bytes, 1, bmp_size, fp) )
    {
        printf("Failed to read the image data!\n");
        free(raw_image_bytes);
        fclose(fp);
        return 0;
    }

    void* read_row = raw_image_bytes; // The row that we are reading
    read_row += (pitch * y_start);
    void* read_pixel = read_row; // The pixel we are reading
    uint32_t* write_pixel = (uint32_t*) image->pixels; // The location to write the read pixel data to
    for( int y = 0; y < real_h; y++ )
    {
        read_pixel = read_row; // We start by reading the first pixel on the read_row
        for( int x = 0; x < real_w; x++ )
        {
            struct RGB pixel_RGB = {0};
            switch(bmp3Header.bitsPerPixel)
            {
                case 8:
                {
                    // If we have color palette entries, then treat each read_pixel as an entry into the palette table
                    if( num_palette_entries )
                    {
                        // Grab the table entry at the corresponding read_pixel (the index) location
                        uint32_t table_entry = palette_entries[ *(uint8_t*)read_pixel ];

                        // A color palette size of 4 indicates we are using BMP v3 or higher, so grab alpha portion
                        if( color_palette_size == 4 )
                        {
                            pixel_RGB.a = (table_entry) >> 24;
                        }

                        pixel_RGB.r = (table_entry << 8) >> 24;
                        pixel_RGB.g = (table_entry << 16) >> 24;
                        pixel_RGB.b = (table_entry << 24) >> 24;
                    }
                    else
                    {
                        printf("Bitmap has 8 bits per pixel but has no color palette entries, BMP is not yet supported!\n");
                    }
                } break;

                case 16:
                {
                    uint16_t pixel_val = *(uint16_t*)read_pixel; // Get the value at our current pixel

                    // If our bitmap is a Windows NT v3 format then the 2 bytes are store in big-endian format
                    // Swap them so that they agree with our little-endian architecture
                    if( bmp3Header.headerSize == head_size )
                    {
                        pixel_val = SwapByteOrder16( pixel_val );
                    }
                
                    // Grab the raw rgb values, these will be scaled to 0-255
                    uint16_t r_raw = (pixel_val & bmp3Header.bitMask.rMask) >> bmp3Header.bitMask.rShift;
                    uint16_t g_raw = (pixel_val & bmp3Header.bitMask.gMask) >> bmp3Header.bitMask.gShift;
                    uint16_t b_raw = (pixel_val & bmp3Header.bitMask.bMask) >> bmp3Header.bitMask.bShift;

                    // Scale our RGB values to a corresponding 0-255 bit scale
                    //      If it is already on a scale of 0-255, it is left unchanged
                    //      If it is on a smaller scale, we get the equivalent color in the 0-255 scale
                    pixel_RGB.r = ((uint32_t)r_raw * 0xFF) / bmp3Header.bitMask.rMax;
                    pixel_RGB.g = ((uint32_t)g_raw * 0xFF) / bmp3Header.bitMask.gMax;
                    pixel_RGB.b = ((uint32_t)b_raw * 0xFF) / bmp3Header.bitMask.bMax;

                    if( is_v4 && bmp3Header.bitMask.aMask )
                    {
                        uint16_t a_raw = (pixel_val & bmp3Header.bitMask.aMask) >> bmp3Header.bitMask.aShift;
                        pixel_RGB.a = ((uint32_t)a_raw * 0xFF) / bmp3Header.bitMask.aMax;
                    }
                } break;

                case 24:
                {
                    pixel_RGB.b = *( (uint8_t*)read_pixel);
                    pixel_RGB.g = *( (uint8_t*)(read_pixel + 1));
                    pixel_RGB.r = *( (uint8_t*)(read_pixel + 2));
                } break;

                case 32:
                {
                    uint32_t pixel_val = *(uint32_t*)read_pixel;

                    if( bmp3Header.headerSize == head_size )
                    {
                        pixel_val = SwapByteOrder32( pixel_val );
                    }

                    uint32_t r_raw = (pixel_val & bmp3Header.bitMask.rMask) >> bmp3Header.bitMask.rShift;
                    uint32_t g_raw = (pixel_val & bmp3Header.bitMask.gMask) >> bmp3Header.bitMask.gShift;
                    uint32_t b_raw = (pixel_val & bmp3Header.bitMask.bMask) >> bmp3Header.bitMask.bShift;

                    // Put colors on a 0-255 scale
                    pixel_RGB.r = (r_raw * 0xFF) / bmp3Header.bitMask.rMax;
                    pixel_RGB.g = (g_raw * 0xFF) / bmp3Header.bitMask.gMax;
                    pixel_RGB.b = (b_raw * 0xFF) / bmp3Header.bitMask.bMax;

                    if( is_v4 && bmp3Header.bitMask.aMask )
                    {
                        uint32_t a_raw = (pixel_val & bmp3Header.bitMask.aMask) >> bmp3Header.bitMask.aShift;
                        pixel_RGB.a = (a_raw * 0xFF) / bmp3Header.bitMask.aMax;
                    }
                } break;

                default:
                {

                } break;
            }

            // Write the RGBA value into the pixel
            WritePixel32( write_pixel, &pixel_RGB );

            // If we are wanting to treat a certain color as transparent, then mark those pixels that match our color_key as transparent
            if( chromaKey && mapped_color_key == *write_pixel )
            {
                *write_pixel |= TRANSPARENT_MASK;
            }

            write_pixel++;
            read_pixel += bytes_pp;
        }
        read_row += (y_dir * pitch);
    }
    
    free(raw_image_bytes);

    if(palette_entries)
        free(palette_entries);

    return 1;
}

