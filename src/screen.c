 
#include <windows.h>
#include <stdio.h>
#include "screen.h"


struct MS_Window CreateMSWindow(WNDPROC windowProc, HINSTANCE hInst, const char* className)
{
    struct MS_Window wnd;

    // Populate the window class structure
    wnd.wc.lpfnWndProc = windowProc;
    wnd.wc.hInstance = hInst;
    wnd.wc.style = CS_HREDRAW | CS_VREDRAW;
    wnd.wc.lpszClassName = className;

    // Set all these to null/0
    wnd.wc.cbClsExtra = 0;
    wnd.wc.cbWndExtra = 0;
    wnd.wc.hIcon = 0;
    wnd.wc.hCursor = 0;
    wnd.wc.hbrBackground = 0;
    wnd.wc.lpszMenuName = 0;
    wnd.wc.hCursor = 0;

    // Register our window class
    if(!RegisterClass(&wnd.wc))
    {
        printf("Window class failed to be created! Error code = %lx\n", GetLastError());
    }

    // Create an instance of the window class
    wnd.hWnd = CreateWindowEx(
            0, // Default window styles
            className, // Window class
            "My window text!", // Text to go on the window title bar
            WS_OVERLAPPEDWINDOW, // Window style (several flags combined... include title bar, border, sytem menu, min/max,...

            // Size and position
            CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, // Let Windows choose

            NULL, // Parent window (there is none, i.e. this is a top level window)
            NULL, // Menu (there is none)
            hInst, // Instance handle for our window
            NULL // Additional application data (there is none)
        );

    if(!wnd.hWnd)
    {
        printf("Window failed to be created! Error code = %lx\n", GetLastError());
    }

    // Grab the device context associated with the particular window
    wnd.deviceContext = GetDC(wnd.hWnd);

    // Get the rectangle representing the client region
    GetClientRect(wnd.hWnd, &wnd.clientRect);

    CreateBitmapImage(&wnd.image, MIN_SCREEN_WIDTH_PX, MIN_SCREEN_HEIGHT_PX);

    return wnd;
}

void UpdateMSWindow(struct MS_Window* wnd)
{
    struct xRect src_local;
    GetBitmapRect(&wnd->image, &src_local);

    // Update the pixels in the client region with the source bitmap image
    int num_scanned_lines = StretchDIBits(
            wnd->deviceContext,
            wnd->clientRect.left,
            wnd->clientRect.top,
            wnd->clientRect.right - wnd->clientRect.left,
            wnd->clientRect.bottom - wnd->clientRect.top,
            src_local.left,
            src_local.top,
            src_local.right - src_local.left,
            src_local.bottom - src_local.top,
            wnd->image.pixels,
            wnd->image.info,
            DIB_RGB_COLORS,
            SRCCOPY
        );

    if(!num_scanned_lines)
    {
        printf("StretchDIBits failed! Error: %lx\n", GetLastError());
    }

    UpdateWindow(wnd->hWnd);
}

int ResizeMSWindow(struct MS_Window* wnd)
{
    RECT new_image_rect;
    GetClientRect(wnd->hWnd, &new_image_rect);

    return ResizeBitmapMemory(&wnd->image,
                              new_image_rect.right - new_image_rect.left,
                              new_image_rect.bottom - new_image_rect.top);
}


void DestroyMSWindow(struct MS_Window* wnd)
{
    ReleaseDC(wnd->hWnd, wnd->deviceContext);
    DestroyBitmapImage(&wnd->image);

    DestroyWindow(wnd->hWnd);
}
















