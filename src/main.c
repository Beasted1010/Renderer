
#include <windows.h>
#include "screen.h"
#include "common.h"

struct MS_Window wnd;
uint8_t running = 1;

void HandlePaintEvent(HWND hwnd, HDC hdc);

// hwnd: Handle to the window
// uMsg: The message code (e.g. WM_SIZE message which indicates the window was resized)
// wParam and lParam: Additional data pertaining to the message -> exact meaning depends on message code (uMsg)
// LRESULT is an integer value that your program returns to Windows, it contains the programs'r esponse to the message
//      The exact meaning of this return value depends on the message code (uMsg)
LRESULT CALLBACK WindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch(uMsg)
    {
        // REGION: Window
        case WM_CREATE:
        {

        } break;

        case WM_SIZE:
        {
            if(!ResizeMSWindow(&wnd))
            {
                printf("Failed to resize the window! Error: %lx\n", GetLastError());
            }
        } break;

        case WM_GETMINMAXINFO:
        {
            MINMAXINFO* min_max_info = (MINMAXINFO*) lParam;

            //min_max_info->ptMinTrackSize.x = MIN_SCREEN_WIDTH_PX;
            //min_max_info->ptMinTrackSize.y = MIN_SCREEN_HEIGHT_PX;
        } break;

        // Triggered by hitting close button (red X) or ALT-F4, and any other closing actions
        case WM_CLOSE:
        {
            DestroyMSWindow(&wnd); // Destroy the specified window 
            PostQuitMessage(0); // Send a quit message to have GetMessage return 0 (thus stop receiving new messages)
            running = 0;
        } break;

        // Triggered after DestroyWindow (called by defalt for WM_CLOSE) and after window removed but before destruction
        case WM_DESTROY:
        {
            running = 0;
        }

        // REGION: Paint
        // The message sent by OS when we must repaint the window
        // NOTE: OS handles painting of title bar and surrounding frame. The "Client Area" is our only responsibility
        // Only the "Update Region" (not whole client region) gets painted, this is the region that has had something change
        // Every time we paint to the update region, we clear the update region so that OS knows we are done with that area
        // E.g. if a user moves a window over our client area, then the update region is under that moved window
        //      The update region does not need to be redrawn since a window is now obstructing its view
        //      A similar situation happens when stretching the window
        // During the paint event you can either paint the entire client region or just the update region (your choice)
        //      Code is simpler if you just paint everything, but more efficient if you only paint update region
        //      The rcPaint member of PAINTSTRUCT will contain the region that needs updated
        case WM_PAINT:
        {
            PAINTSTRUCT ps;
            // All painting occurs between BeginPaint and EndPaint
            HDC hDc = BeginPaint(hWnd, &ps); // Begin painting region

            if( !hDc ) { printf("BeginPaint failed!\n"); }

            HandlePaintEvent(hWnd, hDc);

            EndPaint(hWnd, &ps); // End painting region -> Clears the update region
        } break;

        default:
        {
            return DefWindowProc(hWnd, uMsg, wParam, lParam); // Handle the message in its default manner
        } break;
    }
}

void HandlePaintEvent(HWND hWnd, HDC hDc)
{
    if(!hDc) { printf("Failed to retrieve handle to device context!\n"); }

}

int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
    wnd = CreateMSWindow(WindowProc, hInstance, "My Class");

    MSG msg;

    ShowWindow(wnd.hWnd, nCmdShow);
    struct RGB chroma_key = {.r=255, .g=255, .b=255, .a=0};

    // REGION: Textures
    struct Texture quad_tex = CreateTexture("rsc/Spr_Quadropus.bmp", &chroma_key);
    struct Texture quad_tex2 = CreateTexture("rsc/Spr_Quadropus.bmp", &chroma_key);
    struct Texture quad_tex3 = CreateTexture("rsc/Spr_Quadropus.bmp", &chroma_key);
    //struct Texture Spr_SandWitch = CreateTexture("rsc/Spr_SandWitch.bmp", &chroma_key);
    //struct Texture Bone_Sand = CreateTexture("rsc/T_BoneSand.bmp", &chroma_key);
    //struct Texture venus_tex = CreateTexture("rsc/VENUS.bmp", &chroma_key);
    //struct Texture space_bkg = CreateTexture("rsc/space_background.bmp", &chroma_key);
    //ScaleTexture(&Spr_SandWitch, (1/(float)2));
    ScaleTexture(&quad_tex, (1/(float)3));
    //ScaleTexture(&quad_tex2, (1/(float)2));
    ScaleTexture(&quad_tex3, (1/(float)1));
    ScaleTexture(&quad_tex2, 2);
    //ResizeBitmapMemory(&texture->image, new_width, new_height);

    // REGION: Coordinate System
    /*struct RGB axes_color = {.r=255, .g=255, .b=255, .a=0};
    uint16_t w = wnd.image.w / 5;
    uint16_t h = wnd.image.h / 5;
    struct CoordinateSystem2D coord_sys_2D = CreateCoordinateSystem2D(3, 3, 1, 1, &axes_color);*/

    while(running)
    {
        struct RGB bkgnd = {.r=0, .g=0, .b=0, .a=0};
        WritePixels32(&wnd.image, &bkgnd);
        
        // REGION: Textures
        //PlaceTexture(&quad_tex, &wnd.image, 25, 50, AlphaBinary);
        PlaceTexture(&quad_tex2, &wnd.image, 100, 50, AlphaBinary);
        //PlaceTexture(&quad_tex3, &wnd.image, 200, 50, AlphaBinary);
        //PlaceTexture(&Spr_SandWitch, &wnd.image, 150, 50, AlphaBinary);
        //PlaceTexture(&Bone_Sand, &wnd.image, 100, 400, AlphaBinary);
        //PlaceTexture(&venus_tex, &wnd.image, 50, 400, AlphaIgnore);
        //PlaceTexture(&space_bkg, &wnd.image, 50, 200, AlphaBinary);
        
        //REGION: CoordinateSystem
        //DrawCoordinateSystem2D(&coord_sys_2D, &wnd.image, 50, 50, wnd.image.w / 4, wnd.image.h / 4);
        //DrawCoordinateSystem2D(&coord_sys_2D, &wnd.image, 50, 50, 200, 200);

        UpdateMSWindow(&wnd);

        if(PeekMessage(&msg, wnd.hWnd, 0, 0, PM_REMOVE))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    DestroyMSWindow(&wnd);

    return 0;
}







