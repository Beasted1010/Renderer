
#include <stdint.h>
#include "pixel.h"

    
static inline void CheckCoordinateBounds(uint32_t maxWidth, uint32_t maxHeight, uint32_t* x1, uint32_t* y1, uint32_t* x2, uint32_t* y2)
{
    if(x1) { if(*x1 < 0) *x1 = 0; if(*x1 >= maxWidth) *x1 = maxWidth - 1; }
    if(y1) { if(*y1 < 0) *y1 = 0; if(*y1 >= maxHeight) *y1 = maxHeight - 1; }
    if(x2) { if(*x2 < 0) *x2 = 0; if(*x2 >= maxWidth) *x2 = maxWidth - 1; }
    if(y2) { if(*y2 < 0) *y2 = 0; if(*y2 >= maxHeight) *y2 = maxHeight - 1; }
}

static inline uint8_t OutOfMemoryBounds( struct Bitmap* bitmap, uint32_t* pixel )
{
    uint32_t* upper_bounds = (uint32_t*) bitmap->pixels + ( bitmap->w * bitmap->h );
    if(pixel > upper_bounds)
        return 1;

    if(pixel < (uint32_t*) bitmap->pixels)
        return 1;
}

void ReadPixel32(const uint32_t* pixel, struct RGB* rgb)
{
    if(!pixel)
    {
        printf("ERROR: Trying to read a pixel that doesn't point to anything!\n");
        return;
    }

    rgb->a = (*pixel) >> 24;
    rgb->r = ((*pixel) << 8) >> 24;
    rgb->g = ((*pixel) << 16) >> 24;
    rgb->b = ((*pixel) << 24) >> 24;
}

void WritePixels32(struct Bitmap* bitmap, struct RGB* rgb)
{
    uint32_t* pixel = (uint32_t*) bitmap->pixels; // First pixel

    for(uint32_t i = 0; i < bitmap->h; i++)
    {
        for(uint32_t j = 0; j < bitmap->w; j++)
        {
            *(pixel++) = rgb->a << 24 | rgb->r << 16 | rgb->g << 8 | rgb->b;
        }
    }
}

void WritePixel32(uint32_t* pixel, const struct RGB* rgb)
{
    *(pixel) = rgb->a << 24 | rgb->r << 16 | rgb->g << 8 | rgb->b;
}

static inline void GetNextLinePoint(float m, int* xX, int* xY)
{
    m = m > 0 ? m : -m;

    int x = *xX;
    int y = *xY;

    float err = y - (m * x);

    if( m < 1 )
    {
        err -= m;
        x++;

        if(err < -0.5)
        {
            y++;
        }
    }
    else if(m >= 1)
    {
        err += 1;
        y++;

        if(err > 0.5)
        {
            x++;
        }
    }

    *xX = x;
    *xY = y;
}


static inline void GetNextQuarterCirclePoint(int* xX, int* xY, int r)
{
    int x = *xX;
    int y = *xY;

    float err = (r*r) - (x*x) - (y*y);

    if( x <= y )
    {
        err -= 2*x + 1;
        x++;

        if(err < -0.5)
        {
            y--;
        }
    }
    else if( x >= y )
    {
        err += 2*y - 1;
        y--;

        if(err > 0.5)
        {
            x++;
        }
    }

    *xX = x;
    *xY = y;
}

void DrawQuarterCircle(struct Bitmap* bitmap, struct xCircle* circle, enum Quadrant quadrant, struct RGB* color)
{
    int x = 0, y = circle->radius;

    // Start on top center portion of circle -> (0, radius)
    int start_x = x, start_y = y + circle->radius;
    int y_loc, x_loc;
    int relative_pixel_loc;

    uint32_t* pixel;
    uint32_t* first_pixel = (uint32_t*) bitmap->pixels;

    while( y >= 0 )
    {
        switch(quadrant)
        {
            case TOP_RIGHT:
            {
                x_loc = circle->center.x + x;
                y_loc = circle->center.y - y;
            } break;

            case TOP_LEFT:
            {
                x_loc = circle->center.x - x;
                y_loc = circle->center.y - y;
            } break;

            case BOTTOM_LEFT:
            {
                x_loc = circle->center.x - x;
                y_loc = circle->center.y + y;

            } break;

            case BOTTOM_RIGHT:
            {
                x_loc = circle->center.x + x;
                y_loc = circle->center.y + y;
            } break;

            default:
            {
                printf("Invalid quadrant of %i\n", quadrant);
            } break;
        }

        // This is to cover a bug where circle was being drawn on opposite side of screen (overlapped to other end)
        if(y_loc < 0 || x_loc < 0)
            continue;

        relative_pixel_loc = (bitmap->w * y_loc) + x_loc;
        pixel = first_pixel + relative_pixel_loc;

        if( !OutOfMemoryBounds( bitmap, pixel ) )
        {
            WritePixel32(pixel, color);
        }

        GetNextQuarterCirclePoint(&x, &y, circle->radius);
    }
}

void DrawCircle(struct Bitmap* bitmap, struct xCircle* circle, struct RGB* color)
{
    int x = 0, y = circle->radius;

    // Start on top center portion of circle -> (0, radius)
    int start_x = x, start_y = y + circle->radius;
    int y_loc, x_loc;
    int relative_pixel_loc;

    uint32_t* pixel;
    uint32_t* first_pixel = (uint32_t*) bitmap->pixels;

    while( y >= 0 )
    {
        // We draw only a quarter of a circle and mirror it to the other 3 quadrants
        for(int i = 0; i < 2; i++)
        {
            if(i)
            {
                y_loc = circle->center.y - y;
            }
            else
            {
                y_loc = circle->center.y + y;
            }

            for(int j = 0; j < 2; j++)
            {
                if(j)
                {
                    x_loc = circle->center.x + x;
                }
                else
                {
                    x_loc = circle->center.x - x;
                }

                // This is to cover a bug where circle was being drawn on opposite side of screen (overlapped to other end)
                if(y_loc < 0 || x_loc < 0)
                    continue;

                relative_pixel_loc = (bitmap->w * y_loc) + x_loc;
                pixel = first_pixel + relative_pixel_loc;

                if( !OutOfMemoryBounds( bitmap, pixel ) )
                {
                    WritePixel32(pixel, color);
                }
            }
        }

        GetNextQuarterCirclePoint(&x, &y, circle->radius);
    }
}

void DrawRepeatSymbol(struct Bitmap* bitmap, struct xCircle* circle, uint32_t thickness, struct RGB* color)
{
    uint32_t stop_radius = circle->radius - thickness;
    for( int i = circle->radius; i > stop_radius; i-- )
    {
        circle->radius = i;
        DrawQuarterCircle(bitmap, circle, BOTTOM_LEFT, color);
        DrawQuarterCircle(bitmap, circle, BOTTOM_RIGHT, color);
        DrawQuarterCircle(bitmap, circle, TOP_LEFT, color);
    }

    struct xTriangle arrow_head;
    arrow_head.baseLine.p1.x = circle->center.x;
    arrow_head.baseLine.p1.y = circle->center.y - circle->radius - (thickness * (3/(float)2));
    arrow_head.baseLine.p2.x = arrow_head.baseLine.p1.x;
    arrow_head.baseLine.p2.y = circle->center.y - (circle->radius - (thickness / 2));

    arrow_head.tipPoint.x = arrow_head.baseLine.p1.x + (thickness * 2);
    arrow_head.tipPoint.y = (arrow_head.baseLine.p1.y + arrow_head.baseLine.p2.y) / (float) 2;

    //DrawTriangle(bitmap, &arrow_head, color);
    FillTriangle(bitmap, &arrow_head, color);
}

void FillTriangle(struct Bitmap* bitmap, struct xTriangle* triangle, struct RGB* color)
{
    struct xLine line;
    line.p1.x = triangle->tipPoint.x;
    line.p1.y = triangle->tipPoint.y;

    int start_x = triangle->baseLine.p1.x, start_y = triangle->baseLine.p1.y;
    int end_x = triangle->baseLine.p2.x, end_y = triangle->baseLine.p2.y;

    if( start_x > end_x )
    {
        int temp = start_x;
        start_x = end_x;
        end_x = temp;

        temp = start_y;
        start_y = end_y;
        end_y = temp;
    }

    // Negate due to bitmap being top down and coordinate axes being bottom up
    float top = start_y - end_y;
    float bottom = end_x - start_x;
    float slope;
    if(bottom != 0)
    {
        slope = top / bottom;
    }
    
    int x = 0, y = 0;
    int x_loc = 0, y_loc = 0;

    // TODO: This is currently pretty ugly and can probably be made more clean
    //          I think the issue with mixing the two is within the "GetNextLinePoint" function -> since slop = 0
    //          I don't want to take the time to look more into this. But this is a great place to look when wanting to shorten the code
    if(bottom)
    {
        while( x_loc <= end_x )
        {
            if( slope > 0 )
            {
                y_loc = start_y - y;
            }
            else
            {
                y_loc = start_y + y;
            }

            x_loc = start_x + x;

            line.p2.x = x_loc;
            line.p2.y = y_loc;
            DrawLine(bitmap, &line, color);

            GetNextLinePoint(slope, &x, &y);
        }
    }
    else
    {
        line.p2.x = start_x;
        if(start_y > end_y)
        {
            int temp = start_y;
            start_y = end_y;
            end_y = temp;
        }
        y_loc = start_y;

        //while( (start_y < end_y && y_loc <= end_y) || (start_y > end_y && y_loc >= end_y) )
        while( y_loc <= end_y )
        {
            line.p2.y = y_loc++;
            DrawLine(bitmap, &line, color);
        }
    }
}

void DrawTriangle(struct Bitmap* bitmap, struct xTriangle* triangle, struct RGB* color)
{
    struct xLine line1;
    line1.p1.x = triangle->tipPoint.x;
    line1.p1.y = triangle->tipPoint.y;
    line1.p2.x = triangle->baseLine.p1.x;
    line1.p2.y = triangle->baseLine.p1.y;

    struct xLine line2;
    line2.p1.x = triangle->tipPoint.x;
    line2.p1.y = triangle->tipPoint.y;
    line2.p2.x = triangle->baseLine.p2.x;
    line2.p2.y = triangle->baseLine.p2.y;

    DrawLine(bitmap, &line1, color);
    DrawLine(bitmap, &line2, color);
    DrawLine(bitmap, &triangle->baseLine, color);
}

void DrawHorizontalLine(struct Bitmap* bitmap, uint32_t startX, uint32_t startY, uint32_t endX, struct RGB* color)
{
    //CheckCoordinateBounds(bitmap->w, bitmap->h, &startX, &startY, &endX, NULL);

    uint32_t* pixel = (uint32_t*) bitmap->pixels;
    pixel += (bitmap->w * startY) + startX;

    for(int x = startX; x < endX; x++)
    {
        if( OutOfMemoryBounds( bitmap, pixel ) )
            return;

        WritePixel32(pixel++, color);
    }
}

void DrawVerticalLine(struct Bitmap* bitmap, uint32_t startX, uint32_t startY, uint32_t endY, struct RGB* color)
{
    //CheckCoordinateBounds(bitmap->w, bitmap->h, &startX, &startY, NULL, &endY);

    uint32_t* pixel = (uint32_t*) bitmap->pixels;
    pixel += (bitmap->w * startY) + startX;

    for(int y = startY; y < endY; y++)
    {
        if( OutOfMemoryBounds( bitmap, pixel ) )
            return;

        WritePixel32(pixel, color);
        pixel += bitmap->w;
    }
}

void FillRectangle(struct Bitmap* bitmap, struct xRect* rect, struct RGB* color)
{
    uint32_t height = rect->bottom - rect->top;
    for(int y = 0; y < height; y++)
    {
        DrawHorizontalLine(bitmap, rect->left, y + rect->top, rect->right, color);
    }
}

void DrawRectangle(struct Bitmap* bitmap, struct xRect* rect, struct RGB* color)
{
    DrawHorizontalLine(bitmap, rect->left, rect->top, rect->right, color);
    DrawHorizontalLine(bitmap, rect->left, rect->bottom, rect->right, color);
    DrawVerticalLine(bitmap, rect->left, rect->top, rect->bottom, color);
    DrawVerticalLine(bitmap, rect->right, rect->top, rect->bottom, color);
}

void DrawLine(struct Bitmap* bitmap, struct xLine* xLine, struct RGB* color)
{
    // Line is Vertical
    if( xLine->p1.x == xLine->p2.x )
    {
        DrawVerticalLine(bitmap, xLine->p1.x, xLine->p1.y, xLine->p2.y, color);
        return;
    }

    // Line is Horizontal
    if( xLine->p1.y == xLine->p2.y )
    {
        DrawHorizontalLine(bitmap, xLine->p1.x, xLine->p1.y, xLine->p2.x, color);
        return;
    }


    int start_x = xLine->p1.x, start_y = xLine->p1.y;
    int end_x = xLine->p2.x, end_y = xLine->p2.y;

    if( start_x > end_x )
    {
        int temp = start_x;
        start_x = end_x;
        end_x = temp;

        temp = start_y;
        start_y = end_y;
        end_y = temp;
    }

    uint32_t* pixel;

    // Negate due to bitmap being top down and coordinate axes being bottom up
    float slope = (start_y - end_y) / (float) (end_x - start_x);
    
    int x = 0, y = 0;
    int x_loc, y_loc;
    int relative_pixel_loc;

    while( x < (end_x - start_x) )
    {
        if( slope > 0 )
        {
            y_loc = start_y - y;
        }
        else
        {
            y_loc = start_y + y;
        }

        x_loc = start_x + x;

        //CheckCoordinateBounds(bitmap->w, bitmap->h, &x_loc, &y_loc, 0, 0);
    
        relative_pixel_loc = (bitmap->w * y_loc) + x_loc;

        pixel = ((uint32_t*) bitmap->pixels) + relative_pixel_loc;

        if( OutOfMemoryBounds( bitmap, pixel ) )
            return; 

        WritePixel32(pixel, color);

        GetNextLinePoint(slope, &x, &y);
    }



}

// TODO: This doesn't work well when resizing. I need to revist this and perhaps redesign the idea.
void DrawCoordinateSystem2D(struct CoordinateSystem2D* coordSys, struct Bitmap* bitmap, 
                            uint16_t xStart, uint16_t yStart, uint16_t width, uint16_t height)
{
    // Determine how wide the units should be given the width and how many units are desired
    int x_unit_pixels = width / coordSys->unitsWide;
    int y_unit_pixels = height / coordSys->unitsTall;

    // Draw the axes
    if( coordSys->showGridLines )
    {
        struct RGB grid_line_color = {.r = 127, .g = 127, .b = 127, .a = 0};

        for(int x = xStart; x <= xStart + width; x += x_unit_pixels)
        {
            DrawVerticalLine(bitmap, x, yStart, yStart + height, &grid_line_color);
        }

        for(int y = yStart; y <= yStart + height; y += y_unit_pixels)
        {
            DrawHorizontalLine(bitmap, xStart, y, xStart + width, &grid_line_color);
        }
    }

    // Knowingly drawing over the previous lines so that I don't have to do a constant check for origin in above loops
    DrawHorizontalLine(bitmap, xStart, (coordSys->origin.yUnitLoc * y_unit_pixels) + yStart, xStart + width, 
                       &coordSys->axesColor);
    DrawVerticalLine(bitmap, (coordSys->origin.xUnitLoc * x_unit_pixels) + xStart, yStart, yStart + height, 
                     &coordSys->axesColor);
}

struct CoordinateSystem2D CreateCoordinateSystem2D(uint16_t unitsWide, uint16_t unitsTall,
                                                   uint16_t unitOriginFromLeft, uint16_t unitOriginFromTop,
                                                   struct RGB* axesColor)
{
    struct CoordinateSystem2D coord_sys;

    coord_sys.origin.xUnitLoc = unitOriginFromLeft;
    coord_sys.origin.yUnitLoc = unitOriginFromTop;

    coord_sys.axesColor = *axesColor;
    coord_sys.showGridLines = 1;

    // Set the amount of pixels that make up a single unit in the coordinate system
    coord_sys.unitsWide = unitsWide;
    coord_sys.unitsTall = unitsTall;

    return coord_sys;
}

struct Texture CreateTexture(char* fileLocation, struct RGB* chromaKey)
{
    struct Texture texture;
    
    texture.fileLocation = fileLocation;
    texture.chromaKey = *chromaKey;

    LoadBitmapImage(fileLocation, &texture.image, chromaKey);

    texture.width = texture.image.w < 0 ? -texture.image.w : texture.image.w;
    texture.height = texture.image.h < 0 ? -texture.image.h : texture.image.h;

    texture.rect.left = 0;
    texture.rect.top = 0;
    texture.rect.right = 0;
    texture.rect.bottom = 0;

    return texture;
}

void DestroyTexture(struct Texture* texture)
{
    DestroyBitmapImage(&texture->image);
}

// TODO: Need to handle...
//          - Draw part of texture if it is out of bounds
void PlaceTexture(struct Texture* texture, struct Bitmap* bitmap, uint32_t xPos, uint32_t yPos, enum AlphaSetting alpha)
{
    uint32_t* first_pixel = (uint32_t*) bitmap->pixels;

    uint32_t bitmap_row_size = bitmap->bytesPerPixel * bitmap->w;
    //printf("struct Bitmap row size = %i\n", bitmap_row_size);

    texture->rect.left = xPos;
    texture->rect.top = yPos;
    texture->rect.right = texture->rect.left + texture->width;
    texture->rect.bottom = texture->rect.top + texture->height;

    //for(int i =0; i < texture->width; i++) { *(pixel) = 0; *(pixel++) = 255;}
    //pixel = first_pixel + ((texture->rect.top*bitmap->w) + texture->rect.left);
    // The offset to go from one row in image to next row in image relative to the bitmap
    uint32_t image_row_offset_px = (bitmap->w - texture->width);

    int dst_rem = bitmap->w - xPos;
    if( xPos + texture->width > bitmap->w || xPos < 0 )
    {
        // TODO: Maybe I still want to render just apart of it as the texture leaves the map?
        return;
    }
    else if( yPos + texture->height > bitmap->h || yPos < 0 )
    {
        return;
    }

    // Do we have enough space to put our texture in the bitmap?
    uint32_t scan_len = dst_rem > texture->width ? texture->width : dst_rem;

    uint32_t* src_pixel = (uint32_t*)texture->image.pixels;
    uint32_t* dst_pixel = first_pixel;
    dst_pixel += ((texture->rect.top * bitmap->w) + texture->rect.left);
    if( alpha == AlphaIgnore )
    {
        for( int y = 0; y < texture->height; y++ )
        {
            if( OutOfMemoryBounds(bitmap, dst_pixel) ) // TODO: This currently doesn't work like I want... Just see...
                continue;
            memcpy(dst_pixel, src_pixel, scan_len * sizeof(uint32_t));
            src_pixel += texture->width;
            dst_pixel += bitmap->w;
        }
    }
    else if( alpha == AlphaBinary )
    {
        struct BMPMask bit_mask; 
        struct RGB rgb = {.a=0, .r=0, .g=0, .b=0};
        for(int image_row = 0; image_row < texture->height; image_row++)
        {
            for(int image_col = 0; image_col < texture->width; image_col++)
            {
                if( alpha == AlphaBinary && !(*src_pixel & TRANSPARENT_MASK) )
                {
                    if( OutOfMemoryBounds(bitmap, dst_pixel) ) // TODO: This currently doesn't work like I want... Just see...
                        continue;
                    *dst_pixel = *src_pixel;
                }
                src_pixel++;
                dst_pixel++;
            }
            dst_pixel += image_row_offset_px;
        }
    }
}

// Objective: Calculate the average value of an array of pixels
// Steps:
//       1. Accumulate a sum for each of the bytes in the pixel
//       2. Create a result that has the average of each pixel converted to a byte and moved into correct spot
static inline uint32_t CalculateAveragePixelValue(uint32_t* pixels, uint32_t numPixels)
{
    uint32_t a_acc = 0;
    uint32_t r_acc = 0;
    uint32_t g_acc = 0;
    uint32_t b_acc = 0;

    // Step 1: Accumulate a sum
    for( int i = 0; i < numPixels; i++)
    {
        a_acc += *(pixels) >> 24;
        r_acc += ( *(pixels) << 8 ) >> 24;
        g_acc += ( *(pixels) << 16 ) >> 24;
        b_acc += ( *(pixels) << 24 ) >> 24;
        pixels++;
    }

    // Step 2: Create a result with averaged accumulated values
    uint32_t result = 0;
#define conv_32bit_to_8bit(val, byte) (val << 24) >> (byte++ * 8)
    /*result |= conv_32bit_to_8bit( (a_cc / numPixels), 0);
    result |= conv_32bit_to_8bit( (r_cc / numPixels), 1);
    result |= conv_32bit_to_8bit( (g_cc / numPixels), 2);
    result |= conv_32bit_to_8bit( (b_cc / numPixels), 3);*/
    result |= ( (a_acc / numPixels) << 24 );
    result |= ( (r_acc / numPixels) << 24 ) >> 8;
    result |= ( (g_acc / numPixels) << 24 ) >> 16;
    result |= ( (b_acc / numPixels) << 24 ) >> 24;

    //printf("Average a = %i\n", result >> 24);
    //printf("Average r = %i\n", (result << 8) >> 24);
    //printf("Average g = %i\n", (result << 16) >> 24);
    //printf("Average b = %i\n", (result << 24) >> 24);
    return result;
}

// Objective: To scale the texture by some specified factor
// Steps:
//       1. Create a copy of the original image for reference
//       2. Resize the memory of the texture to be scaled
//       3. Scale the image by applying algorithm to old image and updating pixels in scaled texture image
// IDEA: Support negative? Allow flipping...
void ScaleTexture(struct Texture* texture, float scaleFactor)
{
    // Step 1: Save the original image for reference

    // Create the old image bitmap structure
    struct Bitmap old_image;
    CreateBitmapImage(&old_image, texture->width, texture->height);

    // The pixel that will walk over all of our pixel data
    uint32_t* walk_pixel = (uint32_t*) texture->image.pixels;

    { // Scoped for ridding local variables
        uint32_t* pixel = (uint32_t*) old_image.pixels;

        // Copy over all the data from the texture image to be saved for reference
        for(int y = 0; y < texture->image.h; y++)
        {
            for(int x = 0; x < texture->image.w; x++)
            {
                *(pixel++) = *(walk_pixel++);
            }
        }
        walk_pixel = (uint32_t*) old_image.pixels;
    }

    // Step 2: Resize memory of the texture to be scaled

    texture->height *= scaleFactor;
    texture->width *= scaleFactor;
    ResizeBitmapMemory(&texture->image, texture->width, texture->height);
    uint32_t* scaled_pixel = (uint32_t*) texture->image.pixels;

    // Step 3: Scale the image by the scaleFactor

    float abs_scale_factor = scaleFactor > 0 ? scaleFactor : -scaleFactor;
    // Determine if we are shrinking or growing the image
    if( abs_scale_factor <= 1 )
    { 
        // NOTE: Assuming that scaleFactor is able to be written as a ratio -> 1:pixels_to_average
        uint8_t num_pixels_to_average = 1 / abs_scale_factor;

        // Iterate the scaled texture image's pixels
        for(int y = 0; y < texture->height; y++)
        {
            for(int x = 0; x < texture->width; x++)
            {
                // For each pixel in the shrunken image, there are num_pixels_to_average many from the original averaged together to form the single scaled pixel
                // We will grab surrounding pixels, the selection in x and y direction has 1 overlap, so subtract that out
                uint8_t pixel_counter = (num_pixels_to_average * 2) - 1; // Include the y direction pixels to average
                uint32_t* pixels_to_average = malloc(sizeof(uint32_t) * pixel_counter);

                // -- the counter before indexing in order to put it on a 0 based scale
                pixels_to_average[--pixel_counter] = *(walk_pixel);
                // NOTE: I noticed if we do up to j < num_pixels_to_average / 2, we get a cool little fading effect (but partially due to uninit values of pixels_to_average)
                for(int j = 1; j < num_pixels_to_average; j++) 
                {
                    // Grab pixel in x direction
                    if( !OutOfMemoryBounds(&old_image, walk_pixel + j) ) // TODO: Can I adjust my for loop to ensure I don't go out of bounds?
                    {
                        pixels_to_average[--pixel_counter] = *(walk_pixel + j);
                    }
                    
                    // Grab pixel in y direction
                    if( !OutOfMemoryBounds(&old_image, walk_pixel + (old_image.w * j)) )
                    {
                        pixels_to_average[--pixel_counter] = *(walk_pixel + (old_image.w * j));
                    }

                    if(pixel_counter < 0)
                    {
                        printf("Error with handling pixel_counter! The value became negative!\n");
                    }
                }

                *(scaled_pixel++) = CalculateAveragePixelValue(pixels_to_average, (num_pixels_to_average * 2) - 1);
                walk_pixel += num_pixels_to_average;
                free(pixels_to_average);
            }
            // Skip the rows that we included in the last average
            walk_pixel += old_image.w * (num_pixels_to_average - 1);
        }
    }
    else if( abs_scale_factor > 1 )
    {
        uint8_t num_pixels_to_average = abs_scale_factor;

        // Iterate the scaled texture image's pixels
        for(int y = 0; y < texture->height; y += num_pixels_to_average)
        {
            for(int x = 0; x < texture->width; x += num_pixels_to_average)
            {
                // For each pixel in the shrunken image, there are num_pixels_to_average many from the original averaged together to form the single scaled pixel
                // We will grab surrounding pixels, the selection in x and y direction has 1 overlap, so subtract that out
                uint8_t pixel_counter = 2;//(num_pixels_to_average * 2) - 1; // Include the y direction pixels to average
                uint32_t* pixels_to_average = malloc(sizeof(uint32_t) * pixel_counter);

                *(scaled_pixel) = *(walk_pixel++);

                // TODO: BROKEN!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                for(int j = 1; j < num_pixels_to_average; j++)
                {
                    if( !OutOfMemoryBounds(&old_image, walk_pixel) )
                    {
                        pixels_to_average[--pixel_counter] = *(walk_pixel);
                    }
                    pixels_to_average[--pixel_counter] = *(scaled_pixel);
                    if( !OutOfMemoryBounds(&texture->image, scaled_pixel + 1) )
                    {
                        *(scaled_pixel + 1) = CalculateAveragePixelValue(pixels_to_average, 2);
                    }

                    pixel_counter = 2; // TODO: If only 2, then get rid of variable
                    if( !OutOfMemoryBounds(&old_image, (walk_pixel - 1) + old_image.w) )
                    {
                        pixels_to_average[--pixel_counter] = *((walk_pixel - 1) + old_image.w);
                    }
                    else { break; }
                    pixels_to_average[--pixel_counter] = *(scaled_pixel);
                    if( !OutOfMemoryBounds(&texture->image, scaled_pixel + texture->width) )
                    {
                        *(scaled_pixel + texture->width) = CalculateAveragePixelValue(pixels_to_average, 2);
                    }
                    else { break; }

                    scaled_pixel++;
                }

                //walk_pixel += num_pixels_to_average;
                free(pixels_to_average);
            }
        }
        
    }

}





































/* TODO: Delete, saving here for reference if need
            if(texture->bmi->bmiHeader.biClrUsed)
            {
                rgb.a = texture->bmi->bmiColors[texture->imageData[image_index]].rgbReserved;
                rgb.r = texture->bmi->bmiColors[texture->imageData[image_index]].rgbRed;
                rgb.g = texture->bmi->bmiColors[texture->imageData[image_index]].rgbGreen;
                rgb.b = texture->bmi->bmiColors[texture->imageData[image_index]].rgbBlue;
                image_index += 1;
            }
            else if(texture->bmi->bmiHeader.biCompression == 3)
            {
                // TODO: This is specific for 32 bits (and v4) TODO TODO: This should be read from the header!!! If true biCompression = 3 -> Would be in general then (not just 32 bit or v4)
                bit_mask.aMask = 0xFF000000;
                bit_mask.rMask = 0x00FF0000;
                bit_mask.gMask = 0x0000FF00;
                bit_mask.bMask = 0x000000FF;

                bit_mask.aShift = __builtin_ffs(bit_mask.aMask) - 1;
                bit_mask.rShift = __builtin_ffs(bit_mask.rMask) - 1;
                bit_mask.gShift = __builtin_ffs(bit_mask.gMask) - 1;
                bit_mask.bShift = __builtin_ffs(bit_mask.bMask) - 1;

                bit_mask.aMax = bit_mask.aMask >> bit_mask.aShift;
                bit_mask.rMax = bit_mask.rMask >> bit_mask.rShift;
                bit_mask.gMax = bit_mask.gMask >> bit_mask.gShift;
                bit_mask.bMax = bit_mask.bMask >> bit_mask.bShift;

                uint32_t pixel = 0;
                pixel |= texture->imageData[image_index++]; pixel <<= 8;
                pixel |= texture->imageData[image_index++]; pixel <<= 8;
                pixel |= texture->imageData[image_index++]; pixel <<= 8;
                pixel |= texture->imageData[image_index++];

                uint32_t araw = (pixel & bit_mask.aMask) >> bit_mask.aShift;
                uint32_t rraw = (pixel & bit_mask.rMask) >> bit_mask.rShift;
                uint32_t graw = (pixel & bit_mask.gMask) >> bit_mask.gShift;
                uint32_t braw = (pixel & bit_mask.bMask) >> bit_mask.bShift;


                //rgb.a = (pixel && bit_mask.aMask) >> 24;
                //rgb.r = (pixel && bit_mask.rMask) >> 16;
                //rgb.g = (pixel && bit_mask.gMask) >> 8;
                //rgb.b = (pixel && bit_mask.bMask);
                //rgb.a = pixel >> 24; //&& bit_mask.aMask; // TODO: Alpha channel needs special care? -> On rendering level
                //rgb.r = (pixel << 8) >> 24;// && bit_mask.rMask;
                //rgb.g = (pixel << 16) >> 24;// && bit_mask.gMask;
                //rgb.b = (pixel << 24) >> 24;// && bit_mask.bMask;

                rgb.a = (araw * 0xFF) / bit_mask.aMax;
                rgb.r = (rraw * 0xFF) / bit_mask.rMax;
                rgb.g = (graw * 0xFF) / bit_mask.gMax;
                rgb.b = (braw * 0xFF) / bit_mask.bMax;


            }
            else
            {
                rgb.r = texture->imageData[image_index++];
                rgb.g = texture->imageData[image_index++];
                rgb.b = texture->imageData[image_index++];
            }

            if(rgb.a)
            {

            }
            else
            {
                *(pixel++) = rgb.a << 24 | rgb.r << 16 | rgb.g << 8 | rgb.b;
            }
*/
